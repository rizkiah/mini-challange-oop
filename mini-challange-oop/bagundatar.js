class BangunDatar{
  constructor(props){
     if(this.constructor===BangunDatar){
       throw new Error("Cannot instantiate form Abstract Class")
  }
  }
     luas(){
       console.log();
     }
     keliling(){
       console.log();
     }

}

class Persegi extends BangunDatar{
  //luas s*s
  //keliling 4*s
  constructor(s){
    super();
    this.s = s;
  }

  luas(){
   this.luas = this.s*this.s;
   console.log(`Luas Persegi: ${this.luas}`);
  }

  keliling(){
    this.keliling = 4*this.s;
    console.log(`Keliling Persegi : ${this.keliling}`);
  }
}

class PersegiPanjang extends BangunDatar{
  //luas persegi panjang p*l
  //keliling persegi panjang 2(p+l)
  constructor(p,l){
    super();
    this.p = p;
    this.l = l;
  }
  luas(){
   this.luas = this.p*this.l;
   console.log(`Luas Persegi Panjang: ${this.luas}`);
  }

  keliling(){
    this.keliling = (2*this.p)+(2*this.l);
    console.log(`Keliling Persegi Panjang : ${this.keliling}`);
  }
}

class Segitiga extends BangunDatar{
  //rumus luas segitiga 1/2*a*t
  //rumus keliling segitiga s+s+s
  constructor(a,t,s){
    super();
    this.a = a;
    this.t = t;
    this.s = s;
  }
  luas(){
   this.luas = 1/2*(this.a*this.t);
   console.log(`Luas Segitiga: ${this.luas}`);
  }

  keliling(){
    this.keliling =this.s+this.s+this.s;
    console.log(`Keliling Segitiga : ${this.keliling}`);
  }
}
const newPersegi = new Persegi(3);
newPersegi.luas();
newPersegi.keliling();

const newPersegiPanjang = new PersegiPanjang(3,5);
newPersegiPanjang.luas();
newPersegiPanjang.keliling();

const newSegitiga = new Segitiga(30,10,30);
newSegitiga.luas();
newSegitiga.keliling();
